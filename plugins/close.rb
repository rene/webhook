# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'json'
require 'mail'

# We should close bugs in Debian BTS
post '/close/:package' do
  # Read in data from gitlab
  data = JSON.parse(request.body.read)
  # And put package name into easy variable
  package=params["package"]

  # Loop over all commits
  data["commits"].each do |commit|
    # If they want to close bugs
    if commit["message"] =~ /Closes:\s+#/i
      # Loop over all bugs
      commit["message"].gsub(/.*Closes:\s+/i, "").
        scan(/[Bug#|#](?:(\d{4,8}),?\s*)/i).flatten.each do |bug|
        logger.debug "Closing bug: #{bug}"
        # Send mail to -done@b.d.o
        mail = Mail.new do
          from     commit["author"]["email"]
          to       "#{bug}-done@bugs.debian.org"
          subject  "Bug ##{bug} in #{package} fixed"
          body ERB.new(File.read(File.join($maindir, 'templates/close.erb'))).result(binding)
        end
        # Use local sendmail binary
        mail.delivery_method :sendmail
        mail.deliver!
      end # commit["message"].gsub
    end # if commit["message"]
  end # data["commits"].each
  # Return hardcoded state, gitlab doesn't care
  [200, {}, ['All OK']]
end # post ...

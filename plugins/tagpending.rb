# Copyright (C) 2018, Joerg Jaspert <joerg@debian.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'json'
require 'mail'
require 'soap/rpc/driver'

# FIXME: (maybe) Difference in webhook tagpending compared to old alioth
# tagpending: Old alioth version parses debian/changelog of the repo
# to find latest versions bugcloses and is sending mails for them.

# The webhook version here requires the bug closes to be in the commit
# message instead. We do not want direct access to the gits, so
# parsing a file inside there is kinda hard.

# We should tag bugs with pending
post '/tagpending/:package' do
  # Read in data from gitlab
  data = JSON.parse(request.body.read)
  # Off into background you go, the following can take long,
  # and gitlab doesn't want to wait.
  # And put package name into easy variable
  package=params["package"]
  EM.defer do
    # Create a SOAP something to talk to the BTS
    drv = SOAP::RPC::Driver.
            new(settings.pluginconfig["tagpending"]["server"],
                settings.pluginconfig["tagpending"]["ns"])
    # And we want to ask for status of a bug
    drv.add_method('get_status','bugnumber')
    # With that prepared, loop over all commits
    data["commits"].each do |commit|
      # And if they want to close things,
      if commit["message"] =~ /Closes:\s+#/i
        # loop over all bugs
        commit["message"].gsub(/.*Closes:\s+/i, "").
          scan(/[Bug#|#](?:(\d{4,8}),?\s*)/i).flatten.each do |bug|
          logger.debug("Tagpending for bug ##{bug}")
          # Stuff with SOAP works better with integer where it expects
          # integer
          bug=bug.to_i
          # Check status of this bug
          bdata=drv.get_status(bug)
          # If the bug exists (should hope so) AND it is not done (done
          # field contains an email address == bug is closed)
          if not bdata[bug].nil? and bdata[bug]["done"].length < 2
            logger.info("Bug #{bug} open, tagging")
            # Send mail to submitter, template contains Control: magic
            # for the tagging to happen.
            mail = Mail.new do
              from     commit["author"]["email"]
              to       "#{bug}-submitter@bugs.debian.org"
              subject  "Bug ##{bug} in #{package} marked as pending"
              body ERB.new(File.read(File.join($maindir, 'templates/tagpending.erb'))).result(binding)
            end
            # Use local sendmail binary
            mail.delivery_method :sendmail
            mail.deliver!
          end # if not bdata[bug]
        end # commit["message"].gsub
      end # if commit["message"]
    end # data["commits"].each
  end # EM.defer
  # Return hardcoded state, gitlab doesn't care
  [200, {}, ['All OK']]
end # post ...
